
-- CREATE USER sqladmin WITH PASSWORD 'koalabear';

-- GRANT sqladmin to postgres;

-- CREATE DATABASE asterisk OWNER sqladmin;

SET timezone = 'America/Los_Angeles';


CREATE TABLE sip(
    id SERIAL PRIMARY KEY,
    extension           TEXT NOT NULL,
    label           TEXT NOT NULL,
    siptype           TEXT NOT NULL,
    context           TEXT    NOT NULL,
    siphost           TEXT    NOT NULL,
    sipsecret          TEXT    NOT NULL
);

INSERT INTO sip(extension,label,siptype,context,siphost,sipsecret) VALUES ('2067297888','Matt Birkland','peer','from-sip','sip.telephoneco.com','mysecretpassword123');
INSERT INTO sip(extension,label,siptype,context,siphost,sipsecret) VALUES ('2067297887','Virginia Lau','peer','from-sip','sip.telephoneco.com','anotherpassword!234');
INSERT INTO sip(extension,label,siptype,context,siphost,sipsecret) VALUES ('2067297886','Tim Laughman','peer','from-sip','sip.telephoneco.com','sipsecret1979');

