from fastapi import FastAPI, Request,Form
from fastapi.responses import HTMLResponse,RedirectResponse,JSONResponse
from fastapi.staticfiles import StaticFiles
from fastapi.templating import Jinja2Templates
from fastapi.encoders import jsonable_encoder
import psycopg2
import os


# SQL credentials (normally obscurred, setup for local docker db)
sqluser = "postgres"
sqlpass = "koalabear"
sqldb   = "asterisk"
sqlhost = "db"

# intialize DB connection
conn = psycopg2.connect(f"dbname={sqldb} \
                         user={sqluser} \
                         password={sqlpass} \
                         host={sqlhost}")

cur = conn.cursor()


# Intializ FastAPI async web framework 
app = FastAPI()
app.mount("/static", StaticFiles(directory="static"), name="static")
app.mount("/js", StaticFiles(directory="js"), name="js")
templates = Jinja2Templates(directory="templates")


# pychopg2 postgresql db calls
# add users to postgres
def sip_add_user(label:str,exten:str,siptype:str,
                 context:str,siphost:str,secret:str):
    """
    update DB user values:

    :param label: required
    :param extension
    :param sip_type: required
    :param context: required
    :param siphost: required
    :param secret: required
    """
    cur.execute(f"INSERT INTO sip(extension,label,siptype,context,siphost,sipsecret) \
        VALUES ('{exten}','{label}','{siptype}','{context}','{siphost}','{secret}')")
    conn.commit()

def sip_edit_user():
    pass

def sip_delete_user():
    pass

def sip_get_users():
    """
    Table of User Data linked by Batch ID's

    :param bid: Batch ID
    """
    cur.execute("SELECT * FROM sip;")
    
    return cur.fetchall()





# root page
@app.get("/", response_class=HTMLResponse)
async def web_root_template(request: Request):
    """
    Main page displays template to run JS

    :return: response
    """
    return templates.TemplateResponse("index.html", {"request": request})

# get /sipusers
@app.get("/sipusers", response_class=HTMLResponse)
async def web_root_template(request: Request):
    """
    sip users table

    :return: response
    """
    results = sip_get_users()

    return templates.TemplateResponse("sipusers.html", {"request": request,"siptable":results})


# post to add SQL DB with new users
@app.get("/addsipuser",response_class=HTMLResponse)
async def add_sip_users(request: Request):
    """
    add new user page 
    """
    return templates.TemplateResponse("addsipuser.html", {"request": request})


# post with edit users SQL DB
@app.post("/newsip")
async def post_new_user(label: str = Form(...),exten: str = Form(...),siptype: str = Form(...),
context: str = Form(...),sipserver: str = Form(...),secret: str = Form(...)):
    """
    post new user
    """
    sip_add_user(label,exten,siptype,context,sipserver,secret)

    return RedirectResponse('/sipusers', status_code=301)

# post delete users from SQL DB


# custom REST response
@app.get("/api/example")
async def api_example(request: Request):
    """
    add new user page 
    """
    return {"This": "is a http response in a JSON/JavaScript object format"}


# example passing vard in the URL
@app.get("/api/example/favoritefood/{food}")
async def api_fav_food(request: Request,food):
    """
    add new user page 
    """
    return {"This": "is a JSON response",
            "My FavoriteFood": food}


# example of Python Jinja2 template and JavaScript
@app.get("/phonebook",response_class=HTMLResponse)
async def company_phonebook(request: Request):
    """
    JavaScript parses self hosted API /api/users/phonebook
    """
    return templates.TemplateResponse("phonebook.html", {"request": request})



@app.get("/api/users/phonebook")
async def api_phonebook():
    """
    API returning phonebook
    """
    phonebook = sip_get_users()
    userinfo = list()
    phonevalues = ["ID","Name","Extension","Type","Host","Secret"]

    #phone_comprension

    for user in phonebook:

        userdict = dict(zip(phonevalues,[user[0],user[2],user[1],user[3],user[5]]))

        userinfo.append(userdict)    

    newJSON = {"phonebook":userinfo}

    return JSONResponse(newJSON)